# [5.3.0](https://gitlab.com/to-be-continuous/docker/compare/5.2.2...5.3.0) (2023-08-28)


### Features

* **oidc:** OIDC authentication support now requires explicit configuration(see doc) ([521f918](https://gitlab.com/to-be-continuous/docker/commit/521f918b9f8fab2d23a021211bbdbfacff152c08))

## [5.2.2](https://gitlab.com/to-be-continuous/docker/compare/5.2.1...5.2.2) (2023-07-25)


### Bug Fixes

* **kaniko:** Allow repositories with port numbers to be used for caching ([d17e215](https://gitlab.com/to-be-continuous/docker/commit/d17e215c424fae9b9befc23922f93e23ef273191))

## [5.2.1](https://gitlab.com/to-be-continuous/docker/compare/5.2.0...5.2.1) (2023-07-12)


### Bug Fixes

* **doc:** update typo on documentation [skip ci] ([95245ba](https://gitlab.com/to-be-continuous/docker/commit/95245ba4982663513ac4114878ed8bad0bc07a24))
* **kaniko:** force '--cache-repo' option (strip tag) ([7d4a194](https://gitlab.com/to-be-continuous/docker/commit/7d4a19461953ff81edae0252701abcbbfbc4affe))

# [5.2.0](https://gitlab.com/to-be-continuous/docker/compare/5.1.0...5.2.0) (2023-06-16)


### Features

* **gcp:** add OIDC authentication support for GCP Artifact Registry ([ecac4a6](https://gitlab.com/to-be-continuous/docker/commit/ecac4a692487170adeeccbfdc5d7a97e195c98f2))

# [5.1.0](https://gitlab.com/to-be-continuous/docker/compare/5.0.3...5.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([43ab7e7](https://gitlab.com/to-be-continuous/docker/commit/43ab7e77f94fbd36cf0e0845c5d6efa78c89621b))

## [5.0.3](https://gitlab.com/to-be-continuous/docker/compare/5.0.2...5.0.3) (2023-05-17)


### Bug Fixes

* derive buildah cache from snapshot image ([63016e6](https://gitlab.com/to-be-continuous/docker/commit/63016e61044bf96ba5f1ff239b5e4db6ac2e5b92))

## [5.0.2](https://gitlab.com/to-be-continuous/docker/compare/5.0.1...5.0.2) (2023-05-16)


### Bug Fixes

* **kaniko:** fix $HOME variable ([e213a9e](https://gitlab.com/to-be-continuous/docker/commit/e213a9e24c6712b04af75421ff03e0cf4a52dd34))

## [5.0.1](https://gitlab.com/to-be-continuous/docker/compare/5.0.0...5.0.1) (2023-05-15)


### Bug Fixes

* use $HOME for skopeo credentials for rootless use ([e8b89fd](https://gitlab.com/to-be-continuous/docker/commit/e8b89fdeaada7353998d3b92668873af9cb46b87))

# [5.0.0](https://gitlab.com/to-be-continuous/docker/compare/4.0.0...5.0.0) (2023-05-11)


### Features

* add buildah support for building images ([f8de563](https://gitlab.com/to-be-continuous/docker/commit/f8de56361f8659d8d42144b0b4f26f3bcdf769d0))


### BREAKING CHANGES

* $DOCKER_DIND_BUILD no longer supported (replaced by $`DOCKER_BUILD_TOOL`)

# [4.0.0](https://gitlab.com/to-be-continuous/docker/compare/3.5.3...4.0.0) (2023-04-05)


### Features

* **publish:** redesign publish on prod strategy ([524ccc1](https://gitlab.com/to-be-continuous/docker/commit/524ccc10686991d63aa6f23b40a9b77f68da4cb9))


### BREAKING CHANGES

* **publish:** $PUBLISH_ON_PROD no longer supported (replaced by $DOCKER_PROD_PUBLISH_STRATEGY - see doc)

## [3.5.3](https://gitlab.com/to-be-continuous/docker/compare/3.5.2...3.5.3) (2023-03-28)


### Bug Fixes

* **sbom:** add CycloneDX report ([76c6727](https://gitlab.com/to-be-continuous/docker/commit/76c6727052690354c03604193a6f1ff53bc34e10))

## [3.5.2](https://gitlab.com/to-be-continuous/docker/compare/3.5.1...3.5.2) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([b45e6a2](https://gitlab.com/to-be-continuous/docker/commit/b45e6a2b9cd760a7552fee3c2646a6db91871744))

## [3.5.1](https://gitlab.com/to-be-continuous/docker/compare/3.5.0...3.5.1) (2023-01-04)


### Bug Fixes

* wrong $docker_image_digest if GitLab registry host has a port ([c121756](https://gitlab.com/to-be-continuous/docker/commit/c121756cbf6e24d9890d11720c0a8b43a5fbb902))

# [3.5.0](https://gitlab.com/to-be-continuous/docker/compare/3.4.0...3.5.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider images ([96e2c5d](https://gitlab.com/to-be-continuous/docker/commit/96e2c5df04f633b86ac9463782c37a7dd4e3a549))

# [3.4.0](https://gitlab.com/to-be-continuous/docker/compare/3.3.0...3.4.0) (2022-12-12)


### Features

* semantic release integration ([fabf9b9](https://gitlab.com/to-be-continuous/docker/commit/fabf9b9b3a9c93d08e89b940f22566e7670ee8be))

# [3.3.0](https://gitlab.com/to-be-continuous/docker/compare/3.2.2...3.3.0) (2022-11-29)


### Features

* add a job generating software bill of materials ([ea8ca4e](https://gitlab.com/to-be-continuous/docker/commit/ea8ca4e7c5904e34dda2ee6fb6b33788f4b4544a))

## [3.2.2](https://gitlab.com/to-be-continuous/docker/compare/3.2.1...3.2.2) (2022-10-06)


### Bug Fixes

* Error when generating Trivy report ([2b5ec6e](https://gitlab.com/to-be-continuous/docker/commit/2b5ec6e5fe570b14d0747eb6b65f0f086f1ca75f))

## [3.2.1](https://gitlab.com/to-be-continuous/docker/compare/3.2.0...3.2.1) (2022-10-04)


### Bug Fixes

* **hadolint:** fix shell syntax error ([88de431](https://gitlab.com/to-be-continuous/docker/commit/88de43147892124c85ef6f62a381c8ead7e2929b))

# [3.2.0](https://gitlab.com/to-be-continuous/docker/compare/3.1.1...3.2.0) (2022-10-04)


### Features

* normalize reports ([e8d505f](https://gitlab.com/to-be-continuous/docker/commit/e8d505fea056148389744831a09e9fca978f8f38))

## [3.1.1](https://gitlab.com/to-be-continuous/docker/compare/3.1.0...3.1.1) (2022-09-28)


### Bug Fixes

* support DOCKER_XX_IMAGE with registry port ([54e9e05](https://gitlab.com/to-be-continuous/docker/commit/54e9e05f810b8abb81440cba7dd8ebd7e89644a9))

# [3.1.0](https://gitlab.com/to-be-continuous/docker/compare/3.0.0...3.1.0) (2022-09-20)


### Features

* add custom DOCKER_CONFIG_FILE var ([3201240](https://gitlab.com/to-be-continuous/docker/commit/3201240e9437d7602779fb33252e8dce7e028257))

# [3.0.0](https://gitlab.com/to-be-continuous/docker/compare/2.7.1...3.0.0) (2022-08-05)


### Bug Fixes

* trigger new major release ([580d3e3](https://gitlab.com/to-be-continuous/docker/commit/580d3e3ef93ab570f30b7c90216301fefbdbf84e))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

## [2.7.1](https://gitlab.com/to-be-continuous/docker/compare/2.7.0...2.7.1) (2022-06-20)


### Bug Fixes

* **skopeo:** authenticate with skopeo inspect ([53cf10d](https://gitlab.com/to-be-continuous/docker/commit/53cf10d42aa268650b0b61705f8b241eb3e7d2b4))
* **Trivy:** prefix Trivy report name ([4cec06b](https://gitlab.com/to-be-continuous/docker/commit/4cec06bb5731ced82e3a9fdecfdd82eb54378822))

# [2.7.0](https://gitlab.com/to-be-continuous/docker/compare/2.6.0...2.7.0) (2022-05-20)


### Features

* **Trivy:** allow Trivy to be run in standalone mode ([88348c8](https://gitlab.com/to-be-continuous/docker/commit/88348c8bab10eaf7b2e732ee6c018f50f587af9e))

# [2.6.0](https://gitlab.com/to-be-continuous/docker/compare/2.5.0...2.6.0) (2022-05-19)


### Features

* Make the --vuln-type Trivy argument configurable ([15457c6](https://gitlab.com/to-be-continuous/docker/commit/15457c6e8574b2f4ab4c22163f11118382fa27a2))

# [2.5.0](https://gitlab.com/to-be-continuous/docker/compare/2.4.0...2.5.0) (2022-05-01)


### Features

* configurable tracking image ([b91e936](https://gitlab.com/to-be-continuous/docker/commit/b91e936e028c8002edd8f79beb20d7451c87ead4))

# [2.4.0](https://gitlab.com/to-be-continuous/docker/compare/2.3.3...2.4.0) (2022-04-27)


### Features

* add image digest support ([57998b2](https://gitlab.com/to-be-continuous/docker/commit/57998b26c37086faee0b5524d31917f4f4a3ce53))

## [2.3.3](https://gitlab.com/to-be-continuous/docker/compare/2.3.2...2.3.3) (2022-04-12)


### Bug Fixes

* **Trivy:** restore default Git strategy to allow .trivyignore ([b2fc514](https://gitlab.com/to-be-continuous/docker/commit/b2fc51491d875d65a4998dfa47f124ba11a50615))

## [2.3.2](https://gitlab.com/to-be-continuous/docker/compare/2.3.1...2.3.2) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([b7ac60a](https://gitlab.com/to-be-continuous/docker/commit/b7ac60a595f6a27b3fc0d24c10465ac923b196d0))

## [2.3.1](https://gitlab.com/to-be-continuous/docker/compare/2.3.0...2.3.1) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([4949a87](https://gitlab.com/to-be-continuous/docker/commit/4949a874fb96dedceb0fb16dcd78693d6351dec0))

# [2.3.0](https://gitlab.com/to-be-continuous/docker/compare/2.2.0...2.3.0) (2022-01-10)


### Features

* Vault variant + non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([f1bbac3](https://gitlab.com/to-be-continuous/docker/commit/f1bbac38b305f8bdda946d8372338c5293da7f59))

# [2.2.0](https://gitlab.com/to-be-continuous/docker/compare/2.1.2...2.2.0) (2021-11-24)


### Features

* add configurable metadata variable with OCI recommended labels ([d3630f9](https://gitlab.com/to-be-continuous/docker/commit/d3630f9356ba3c9934f972ca728f562e3d015019))

## [2.1.2](https://gitlab.com/to-be-continuous/docker/compare/2.1.1...2.1.2) (2021-10-19)


### Bug Fixes

* **trivy:** ignore unfixed security issues by default ([f9a1602](https://gitlab.com/to-be-continuous/docker/commit/f9a160201fe11a1ac7d125b5fa9aa181c4599fd5))

## [2.1.1](https://gitlab.com/to-be-continuous/docker/compare/2.1.0...2.1.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([5596daf](https://gitlab.com/to-be-continuous/docker/commit/5596daf34cc9c64d73405ef490fe75c7ae50a177))

## [2.0.0](https://gitlab.com/to-be-continuous/docker/compare/1.2.3...2.0.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([f74d9ef](https://gitlab.com/to-be-continuous/docker/commit/f74d9ef2de5b4dc204b519e14c47862ea2b73b33))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.3](https://gitlab.com/to-be-continuous/docker/compare/1.2.2...1.2.3) (2021-07-21)

### Bug Fixes

* variable name ([1aed76f](https://gitlab.com/to-be-continuous/docker/commit/1aed76f287ff27e35233941e35cbac1e8ea9d2ff))

## [1.2.2](https://gitlab.com/to-be-continuous/docker/compare/1.2.1...1.2.2) (2021-07-05)

### Bug Fixes

* update skopeo credentials ([559961f](https://gitlab.com/to-be-continuous/docker/commit/559961f9f3c30e241a049ee4ea94e9050be49592))

## [1.2.1](https://gitlab.com/to-be-continuous/docker/compare/1.2.0...1.2.1) (2021-06-25)

### Bug Fixes

* permission on reports directory ([2d2f360](https://gitlab.com/to-be-continuous/docker/commit/2d2f360420b13bbdb71c11910ac8214b74a15901))

## [1.2.0](https://gitlab.com/to-be-continuous/docker/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([405fdcc](https://gitlab.com/to-be-continuous/docker/commit/405fdcc65ef1e95cb5997c610266c1422a06802e))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/docker/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([fc9a1ad](https://gitlab.com/Orange-OpenSource/tbc/docker/commit/fc9a1adc498209ea9fa7c6eb64831f6e1abe857f))

## 1.0.0 (2021-05-06)

### Features

* initial release ([0b00fba](https://gitlab.com/Orange-OpenSource/tbc/docker/commit/0b00fba9a515d4ea10c35cf38abc01c217f0979a))
